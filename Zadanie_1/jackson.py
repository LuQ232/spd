def jackson_alghoritm(il_zadan, termin, czas):
    # sortujemy względem terminu i analogicznie zamieniamy miejscami czas
    for i in range(0, il_zadan):
        for j in range(1, il_zadan):
            if termin[j - 1] > termin[j]:
                termin[j - 1], termin[j] = termin[j], termin[j - 1]  # zamiana termin[j-1] z termin[j]
                czas[j - 1], czas[j] = czas[j], czas[j - 1]  # analogiczna zamiana jak wyzej

    # dane wyjściowe
    output = []

    for i in range(0, il_zadan):
        # dla pierwszego wykonania porównujemy z zerem aby uniknąć przekroczenia tablicy -> (output[-1])
        if i == 0:
            output.append(max(termin[i], 0) + czas[i])
        # dalej normalnie porównujemy z output[i-1]
        else:
            output.append(max(termin[i], output[i - 1]) + czas[i])

    # zwrocenie wyniku
    return output.pop()