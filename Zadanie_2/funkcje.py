def konwersja_str_int(s):
    # rzutowanie str->int wszystkich elementow w liscie
    for i in range(len(s)):
        s[i] = int(s[i])
    return s


def odczyt_pliku(nazwa_pliku):
    # otwarcie pliku w trybie do odczytu
    f = open(nazwa_pliku, 'r')
    # zmienne - termin dostepnosci, czas, ilosc zadan oraz licznik pomocniczy
    r = []
    p = []
    q = []
    n = 0
    # odczyt linii -> kazda kolejna linia jest umieszczana jako osobny element listy
    wszystkie_linie = f.readlines()
    # rstrip('\n') - pomija znak konca linii, split(' ') - pomija spacje i dzieli elementy na pojedyncze elementy
    for licznik, linia in enumerate(wszystkie_linie):
        if licznik == 0:
            n = linia.rstrip('\n')
        else:
            s = linia.rstrip('\n').split(' ')
            r.append(s[0])
            p.append(s[1])
            q.append(s[2])
    # zamkniecie pliku na koniec dzialania
    f.close()
    # zwrocenie danych
    return int(n), konwersja_str_int(r), konwersja_str_int(p), konwersja_str_int(q)


def wyswietl_plik(n, r, p, q):
    # dzieki temu wektory wypisuja sie jako kolumny, obok siebie
    for i in range(0, len(r) + 1):
        if i == 0:
            print(n)
        elif i > 0:
            print(r[i - 1], p[i - 1], q[i - 1])


def spr_wynik(nazwa_pliku, rzeczywisty_wynik):
    f = open(nazwa_pliku, 'r')
    poprawny_wynik = konwersja_str_int(f.readlines())[0]
    f.close()
    if poprawny_wynik == rzeczywisty_wynik:
        return True
    else:
        return False


def schrage_alghoritm(n, r, p, q):
    """
    ~~~~~~~~~~DANE~~~~~~~~~~
    n - liczba zadań
    r - termin dostępności
    p - czas obsługi
    q - czas dostarczenia~
    ~~~~~~~~~~~~~~~~~~~~~~~~

    ~~~~~~~~~~SZUKANE~~~~~~~
    permutacja[] - permutacja wykonania zadań na maszynie
    Cmax - maksymalny z terminów dostarczenia zadań
    ~~~~~~~~~~~~~~~~~~~~~~~~

    ~~~~~~~~~~POMOCNICZE~~~~~~~
    t - chwila czasowa
    k - pozycja w permutacji
    N - zbiór zadań nieuszeregowanych
    G - zbiór zadań gotowych do realizacji
    ~~~~~~~~~~~~~~~~~~~~~~~~
    """

    #INICJACJA ZMIENNYCH
    t = 0
    k = 0
    Cmax = 0
    G = []
    permutacja = []
    N = [(i, r[i]) for i in range(0, n)]
    N = sorted(N, key=lambda x: x[1], reverse=False)
    #ALGORYTM
    while len(G) != 0 or len(N) != 0:
        while len(N) != 0 and N[0][1] <= t:
            e = N[0][0]
            G.append((e, q[e]))
            N.pop(0)
        if len(G) == 0:
            t = N[0][1]
        else:
            G = sorted(G, key=lambda x: x[1], reverse=True)
            e = G[0][0]
            G.pop(0)
            k = k + 1
            permutacja.insert(k, e)
            t = t + p[e]
            Cmax = max(Cmax, t + q[e])
    return Cmax
