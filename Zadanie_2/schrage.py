def schrage_alghoritm(n, r, p, q):
    """
    ~~~~~~~~~~DANE~~~~~~~~~~
    n - liczba zadań
    r - termin dostępności
    p - czas obsługi
    q - czas dostarczenia~
    ~~~~~~~~~~~~~~~~~~~~~~~~

    ~~~~~~~~~~SZUKANE~~~~~~~
    permutacja[] - permutacja wykonania zadań na maszynie
    Cmax - maksymalny z terminów dostarczenia zadań
    ~~~~~~~~~~~~~~~~~~~~~~~~

    ~~~~~~~~~~POMOCNICZE~~~~~~~
    t - chwila czasowa
    k - pozycja w permutacji
    N - zbiór zadań nieuszeregowanych
    G - zbiór zadań gotowych do realizacji
    ~~~~~~~~~~~~~~~~~~~~~~~~
    """

    #INICJACJA ZMIENNYCH
    t = 0
    k = 0
    Cmax = 0
    G = []
    permutacja = []
    N = [(i, r[i]) for i in range(0, n)]
    N = sorted(N, key=lambda x: x[1], reverse=False)

    #ALGORYTM
    while len(G) != 0 or len(N) != 0:
        while len(N) != 0 and N[0][1] <= t:
            e = N[0][0]
            G.append((e, q[e]))
            N.pop(0)
        if len(G) == 0:
            t = N[0][1]
        else:
            G = sorted(G, key=lambda x: x[1], reverse=True)
            e = G[0][0]
            G.pop(0)
            k = k + 1
            permutacja.insert(k, e)
            t = t + p[e]
            Cmax = max(Cmax, t + q[e])
    return Cmax
