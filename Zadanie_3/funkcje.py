import math

def konwersja_str_int(s):
    # rzutowanie str->int wszystkich elementow w liscie
    for i in range(len(s)):
        s[i] = int(s[i])
    return s


def odczyt_pliku(nazwa_pliku):
    # otwarcie pliku w trybie do odczytu
    f = open(nazwa_pliku, 'r')
    # zmienne - termin dostepnosci, czas, ilosc zadan oraz licznik pomocniczy
    r = []
    p = []
    q = []
    n = 0
    # odczyt linii -> kazda kolejna linia jest umieszczana jako osobny element listy
    wszystkie_linie = f.readlines()
    # rstrip('\n') - pomija znak konca linii, split(' ') - pomija spacje i dzieli elementy na pojedyncze elementy
    for licznik, linia in enumerate(wszystkie_linie):
        if licznik == 0:
            n = linia.rstrip('\n')
        else:
            s = linia.rstrip('\n').split(' ')
            r.append(s[0])
            p.append(s[1])
            q.append(s[2])
    # zamkniecie pliku na koniec dzialania
    f.close()
    # zwrocenie danych
    return int(n), konwersja_str_int(r), konwersja_str_int(p), konwersja_str_int(q)


def wyswietl_plik(n, r, p, q):
    # dzieki temu wektory wypisuja sie jako kolumny, obok siebie
    for i in range(0, len(r) + 1):
        if i == 0:
            print(n)
        elif i > 0:
            print(r[i - 1], p[i - 1], q[i - 1])


def spr_wynik(nazwa_pliku, rzeczywisty_wynik):
    f = open(nazwa_pliku, 'r')
    poprawny_wynik = konwersja_str_int(f.readlines())[0]
    f.close()
    if poprawny_wynik == rzeczywisty_wynik:
        return True
    else:
        return False


def schrage_prmt_alghoritm(n, r, p, q):
    """
    ~~~~~~~~~~~~~~~~~~~DANE~~~~~~~~~~~~~~~~~~~
    n - liczba zadań
    r - termin dostępności
    p - czas obsługi (czas wykonania zadania)
    q - czas dostarczenia
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    ~~~~~~~~~~~~~~~~~~~SZUKANE~~~~~~~~~~~~~~~~~~~~~~
    Cmax - maksymalny z terminów dostarczenia zadań
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    ~~~~~~~~~~~~~~POMOCNICZE~~~~~~~~~~~~~~~~~~~~~
    t - chwila czasowa
    l - aktualnie wykonywane zadanie
    N - zbiór zadań nieuszeregowanych
    G - zbiór zadań gotowych do realizacji
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    """

    #INICJACJA ZMIENNYCH
    t = 0
    Cmax = 0
    G = []
    N = [(i, r[i]) for i in range(0, n)]
    N = sorted(N, key=lambda x: x[1], reverse=False)
    l = 0
    q0 = math.inf
    #ALGORYTM
    while len(G) != 0 or len(N) != 0:
        while len(N) != 0 and N[0][1] <= t:
            e = N[0][0]
            G.append((e, q[e]))
            N.pop(0)
            #Róznica pomiedzy schrage a schrage z podzialem:
            #Gdy do zbioru zadań gotowych dodawane jest zadanie e(wyżej), sprawdzamy czy
            #ma większy czas dostarczenia od zadania (l) czyli aktualnie znajdującego sie w maszynie
            #Jeżeli tak to przerywamy aktualne zadanie a pozostała ( t-r[e]) częsc zadania
            #wrzucana jest spowrotem do zadań gotowych do realizacji.
            if q[e] > q[l]:
                p[l] = t - r[e]
                t = r[e]
                if p[l] > 0:
                    G.append((l, q[l]))
        if len(G) == 0:
            t = N[0][1]
        else:
            max_q = max(G, key=lambda x: x[1])
            e = max_q[0]
            G.pop(G.index(max_q))
            l = e
            t = t + p[e]
            Cmax = max(Cmax, t + q[e])
    return Cmax
