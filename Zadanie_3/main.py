import funkcje as fu

for i in range(1, 10):
    il_zadan, termin_dostepnosci, czas_wykonania, czas_dostarczenia  = fu.odczyt_pliku(f'Input/SCHRAGE{i}.DAT')
    #fu.wyswietl_plik(il_zadan, termin_dostepnosci, czas_wykonania, czas_dostarczenia)
    wynik = fu.schrage_prmt_alghoritm(il_zadan, termin_dostepnosci, czas_wykonania, czas_dostarczenia)

    if fu.spr_wynik(f'Output/SCHRAGE{i}.DAT', wynik) is True:
        print(f'SCHRAGE_Z_PODZIALEM{i} ->', wynik, '\u2713')
    else:
        print(f'SCHRAGE_Z_PODZIALEM{i} ->', wynik, '\u2717')
