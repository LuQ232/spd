def schrage_prmt_alghoritm(n, r, p, q):
    """
    ~~~~~~~~~~~~~~~~~~~DANE~~~~~~~~~~~~~~~~~~~
    n - liczba zadań
    r - termin dostępności
    p - czas obsługi (czas wykonania zadania)
    q - czas dostarczenia
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    ~~~~~~~~~~~~~~~~~~~SZUKANE~~~~~~~~~~~~~~~~~~~~~~
    Cmax - maksymalny z terminów dostarczenia zadań
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    ~~~~~~~~~~~~~~POMOCNICZE~~~~~~~~~~~~~~~~~~~~~
    t - chwila czasowa
    l - aktualnie wykonywane zadanie
    N - zbiór zadań nieuszeregowanych
    G - zbiór zadań gotowych do realizacji
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    """

    #INICJACJA ZMIENNYCH
    t = 0
    Cmax = 0
    G = []
    N = [(i, r[i]) for i in range(0, n)]
    N = sorted(N, key=lambda x: x[1], reverse=False)
    l = 0
    q0 = math.inf
    #ALGORYTM
    while len(G) != 0 or len(N) != 0:
        while len(N) != 0 and N[0][1] <= t:
            e = N[0][0]
            G.append((e, q[e]))
            N.pop(0)
            #Róznica pomiedzy schrage a schrage z podzialem:
            #Gdy do zbioru zadań gotowych dodawane jest zadanie e(wyżej), sprawdzamy czy
            #ma większy czas dostarczenia od zadania (l) czyli aktualnie znajdującego sie w maszynie
            #Jeżeli tak to przerywamy aktualne zadanie a pozostała ( t-r[e]) częsc zadania
            #wrzucana jest spowrotem do zadań gotowych do realizacji.
            if q[e] > q[l]:
                p[l] = t - r[e]
                t = r[e]
                if p[l] > 0:
                    G.append((l, q[l]))
        if len(G) == 0:
            t = N[0][1]
        else:
            max_q = max(G, key=lambda x: x[1])
            e = max_q[0]
            G.pop(G.index(max_q))
            l = e
            t = t + p[e]
            Cmax = max(Cmax, t + q[e])
    return Cmax
